<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title></title>
</head>
<body>
<a href="/books.do">Liste</a>

<h1>Detail</h1>

<p>
    Id : <c:out value="${book.id}"/><br>
    Author :  <c:out value="${book.author}"/><br>
    Title : <c:out value="${book.title}"/><br>
</p>

<form:form modelAttribute="book" method="POST" action="/books/${book.id}.do">
    <label for="bookAutor">Autor </label>
    <form:input path="author" id="bookAutor" />
    <br>

    <label for="bookName">Title </label>
    <form:input path="title" id="bookName" />
    <br>

    <button type="submit"><spring:message code="label.register"/></button>

</form:form>

<a href="/books/delete/${book.id}.do">Delete this book</a>


</body>
</html>
