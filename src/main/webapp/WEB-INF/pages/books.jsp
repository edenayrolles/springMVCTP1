<%@ page import="com.sqli.jacademie.domain.Book" %>
<%@ page import="java.util.Collection" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title></title>
</head>
<body>

    <h1>Liste des livres</h1>

    <ul>
        <c:forEach var="currentBook" items="${books}">
            <li>
                <a href="/books/${currentBook.id}.do">
                        <c:out value="${currentBook.id}" /> :
                                <c:out value="${currentBook.author}" /> :
                                <c:out value="${currentBook.title}" />
                </a>
            </li>
        </c:forEach>
    </ul>


    <form:form modelAttribute="book" method="POST" action="books.do">
        <label for="bookAutor">Autor </label>
        <form:input path="author" id="bookAutor" />
        <br>

        <label for="bookName">Title </label>
        <form:input path="title" id="bookName" />
        <br>

        <button type="submit"><spring:message code="label.register"/></button>

    </form:form>
</body>
</html>
