<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<spring:message code="label.hello" /> <c:out value="${nameAttr}" />

<a href="/helloName.do?name=Eric&lang=en">En</a>
</body>
</html>