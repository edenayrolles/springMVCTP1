package com.sqli.jacademie.service;

import com.sqli.jacademie.DAO.BookDAO;
import com.sqli.jacademie.domain.Book;
import com.sqli.jacademie.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by edenayrolles on 22/01/2016.
 */
@Service
@Transactional
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Collection<Book> getBooks() {

        return bookRepository.findAll();
    }

    @Override
    public Book addBook(Book book) {
        bookRepository.save(book);
        return book;
    }

    @Override
    public Book findBookById(int id) {
        return bookRepository.findById(id);
    }

    @Override
    public Book updateBook(Book book) {
        bookRepository.save(book);
        return book;
    }

    @Override
    public boolean removeBookById(int bookId) {
        bookRepository.delete(bookRepository.findById(bookId));
        return true;
    }
}
