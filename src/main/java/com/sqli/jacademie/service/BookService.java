package com.sqli.jacademie.service;

import com.sqli.jacademie.domain.Book;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by edenayrolles on 22/01/2016.
 */
public interface BookService {

    public Collection<Book> getBooks();

    public Book addBook(Book book);

    public Book findBookById(int id);

    public Book updateBook(Book book);

    public boolean removeBookById(int bookId);
}
