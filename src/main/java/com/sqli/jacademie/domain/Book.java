package com.sqli.jacademie.domain;

import javax.persistence.*;

/**
 * Created by edenayrolles on 22/01/2016.
 */

@Entity

@Table(name = "books", schema = "public", catalog = "postgres")
public class Book {

    @Id
    @GeneratedValue
    @Column(name = "book_id", nullable = false, insertable = true, updatable = false)
    private int id;

    @Column(name = "title", nullable = true, insertable = true, updatable = true)
    private String title;

    @Column(name = "author", nullable = true, insertable = true, updatable = true)
    private String author;

    public Book() {
    }

    public Book(int id, String title, String author) {
        this.id = id;
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
