package com.sqli.jacademie.controller;

import com.sqli.jacademie.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by edenayrolles on 15/01/2016.
 */
@Controller
public class HelloController {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String helloWorld() {
        return "hello-world";
    }


    @RequestMapping(value = "/helloName", method = RequestMethod.GET)
    public  String helloWorldWithName(@RequestParam("name") String nameInCtrl, Model model) {

        model.addAttribute("nameAttr", nameInCtrl);

        return "hello-name";
    }



    @ModelAttribute("user")
    public User getUser() {
        return new User();
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registerPage() {

        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView submitForm(@ModelAttribute(value="user") User user) {


        return new ModelAndView("user-registered", "user", user);
    }
}
