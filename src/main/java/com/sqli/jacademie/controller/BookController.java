package com.sqli.jacademie.controller;

import com.sqli.jacademie.domain.Book;
import com.sqli.jacademie.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by edenayrolles on 22/01/2016.
 */
@Controller
@RequestMapping(value = "/books")
public class BookController {

    @Autowired
    private BookService bookService;

    //List
    //Post => Ajout
    //Get avec id => detail
    //Put avec Id => modification
    // Delet avec id => suppression

    @ModelAttribute("book")
    public Book getUser() {
        return new Book();
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getBooks(Model model) {

        model.addAttribute("books", bookService.getBooks());

        return "books";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String addBook(@ModelAttribute(value = "book") Book book, Model model) {

        bookService.addBook(book);

        model.addAttribute("books", bookService.getBooks());

        return "books";
    }






    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String findBookByIdBook(@PathVariable(value = "id") int bookId, Model model) {

        model.addAttribute("book", bookService.findBookById(bookId));

        return "book";
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public String updateBook(@PathVariable(value = "id") int bookId, @ModelAttribute(value = "book") Book book, Model model) {

        book.setId(bookId);
        book = bookService.updateBook(book);

        model.addAttribute("book", book);

        return "book";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String updateBook(@PathVariable(value = "id") int bookId, Model model) {

        bookService.removeBookById(bookId);

        return "redirect:/books.do";
    }


    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }
}
