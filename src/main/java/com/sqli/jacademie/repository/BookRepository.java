package com.sqli.jacademie.repository;

import com.sqli.jacademie.domain.Book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Created by edenayrolles on 22/01/2016.
 */
public interface BookRepository extends JpaRepository<Book, Integer> {

    @Query("SELECT a FROM Book a  WHERE a.id = :id")
    Book findById(@Param("id") int id);
}
